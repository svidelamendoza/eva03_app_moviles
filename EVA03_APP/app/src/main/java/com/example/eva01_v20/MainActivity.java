package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.UsuarioDao;

public class MainActivity extends AppCompatActivity {

    EditText correo;
    EditText password;
    Button login;
    String correoUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Método en action Bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        correo = findViewById(R.id.txtmail);
        password = findViewById(R.id.txtcontrasena);

    }



    //Método Botón Registrarse
    public void Registrarse(View view){
            Intent registrarse = new Intent(this, MainActivity_Register.class);
            startActivity(registrarse);

    }

    //Método Botón login
    public void Log(View view) {

        if(validaCampos()){
            UsuarioDao usuarioDao = new UsuarioDao(this);
            correoUsuario = correo.getText().toString().trim();
            if(usuarioDao.login(this,correoUsuario,password.getText().toString().trim())){
                Intent entrar = new Intent(this, MainActivity_Menu.class);
                entrar.putExtra("correo",correoUsuario);
                startActivity(entrar);
            }else{
                Toast.makeText(this, "Revise sus credenciales o cree su usuario", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validaCampos() {
        boolean valido = false;

        if (correo.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su correo", Toast.LENGTH_SHORT).show();
        }else if(password.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su contraseña", Toast.LENGTH_SHORT).show();
        }else {
            valido = true;
        }

        return valido;
    }

}

