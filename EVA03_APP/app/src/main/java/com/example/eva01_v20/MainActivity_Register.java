package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.UsuarioDao;
import com.example.eva01_v20.data.entidades.Usuario;

public class MainActivity_Register extends AppCompatActivity {

    EditText correo;
    EditText nombre;
    EditText apellido;
    EditText fechaNacimiento;
    EditText estatura;
    EditText password;
    EditText confirmaPass;
    Usuario usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__register);

        nombre = findViewById(R.id.txtname);
        apellido = findViewById(R.id.txtlastname);
        correo = findViewById(R.id.txtmail);
        estatura = findViewById(R.id.txtheight);
        fechaNacimiento = findViewById(R.id.txtbdate);
        password = findViewById(R.id.txtpassword);
        confirmaPass = findViewById(R.id.txtconfirmarp);
    }

    //Método para botón Registrar
    public void Registrar(View view){

        if (validaCampos()){

            if(registrarUsuario()){
                Toast.makeText(this, "Usuario registrado", Toast.LENGTH_SHORT).show();

                Intent registrar = new Intent(this, MainActivity.class);
                startActivity(registrar);
            }
        }
    }

    private boolean registrarUsuario() {

        boolean registro = false;

        usuario = new Usuario();
        usuario.setNombre(nombre.getText().toString().trim());
        usuario.setApellido(apellido.getText().toString().trim());
        usuario.setCorreo(correo.getText().toString().trim());
        usuario.setEstatura(Double.parseDouble(estatura.getText().toString().trim()));
        usuario.setFecha_nacimiento(fechaNacimiento.getText().toString().trim());
        usuario.setClave(password.getText().toString().trim());

        UsuarioDao usuarioDao = new UsuarioDao(getApplicationContext());

        if(usuarioDao.insertarUsuario(getApplicationContext(),usuario)){
            registro = true;
        }

        return registro;
    }

    private boolean validaCampos(){
        boolean valido = false;

        if(nombre.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su nombre", Toast.LENGTH_SHORT).show();
        }else if(apellido.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su apellido", Toast.LENGTH_SHORT).show();
        }else if(correo.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su correo", Toast.LENGTH_SHORT).show();
        }else if(estatura.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su estatura", Toast.LENGTH_SHORT).show();
        }else if(fechaNacimiento.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su fecha de nacimiento", Toast.LENGTH_SHORT).show();
        }else if(password.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su contraseña", Toast.LENGTH_SHORT).show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(correo.getText().toString().trim()).matches()){
            Toast.makeText(this, "Debe ingresar un correo valido", Toast.LENGTH_SHORT).show();
        }else if(!validaContrasenia(password.getText().toString(),confirmaPass.getText().toString())){
            Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
        }else {
            valido = true;
        }

        return valido;
    }
    private boolean validaContrasenia(String password,String confirmaPass) {
        if (password.trim().equals(confirmaPass.trim())){
            return true;
        }else {
            return false;
        }
    }
}