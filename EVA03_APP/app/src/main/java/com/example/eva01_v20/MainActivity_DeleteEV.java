package com.example.eva01_v20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.Toast;

import com.example.eva01_v20.adapter.EvaluacionesAdapter;
import com.example.eva01_v20.data.ConexionSqLiteHelper;
import com.example.eva01_v20.data.dao.EvaluacionDao;

public class MainActivity_DeleteEV extends AppCompatActivity {

    Cursor evaluaciones;
    RecyclerView mRecyclerView;
    EvaluacionesAdapter evaluacionesAdapter;
    RecyclerView.LayoutManager manager;
    EvaluacionDao evaluacionDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__delete_e_v);

        cargaEva();

        mRecyclerView = findViewById(R.id.recyclerView2);
        mRecyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(this);
        evaluacionesAdapter = new EvaluacionesAdapter(this, evaluaciones);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                eliminaEvaluacion((int) viewHolder.itemView.getTag());
            }
        }).attachToRecyclerView(mRecyclerView);
    }

    private void eliminaEvaluacion(int id) {
        evaluacionDao = new EvaluacionDao(getApplicationContext());
        if (evaluacionDao.deleteEvaluacion(id)) {
            cargaEva();
            evaluacionesAdapter.swapCursor(evaluaciones);
            Toast.makeText(this, "Registro eliminado correctamente", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Problema al eliminar el registro", Toast.LENGTH_SHORT).show();
        }
    }

    public void cargaEva() {
        Toast.makeText(this, "Carga Todas", Toast.LENGTH_SHORT).show();
        ConexionSqLiteHelper conn = new ConexionSqLiteHelper(getApplicationContext());
        SQLiteDatabase db = conn.getReadableDatabase();

        Cursor cursor = db.query("evaluaciones", null, null, null, null, null, null);


        evaluaciones = cursor;
    }

}