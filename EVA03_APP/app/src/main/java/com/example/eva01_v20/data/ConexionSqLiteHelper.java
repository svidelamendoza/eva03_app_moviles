package com.example.eva01_v20.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ConexionSqLiteHelper extends SQLiteOpenHelper {

    static final String NOMBRE_BD = "Gymfitness";
    static final int DB_VERSION = 1;

    final String TABLA_USUARIOS = "usuarios";

    final String KEY_ID_USUARIO ="id";
    final String KEY_CORREO ="correo";
    final String KEY_NOMBRE ="nombre";
    final String KEY_APELLIDO ="apellido";
    final String KEY_FECHA_NACIMIENTO ="fecha_nacimiento";
    final String KEY_ESTATURA ="estatura";
    final String KEY_CLAVE ="clave";

    final String TABLA_EVALUACIONES = "evaluaciones";

    final String KEY_ID ="id";
    final String KEY_PESO ="peso";
    final String KEY_IMC ="imc";
    final String KEY_FECHA ="fecha";
    final String KEY_IDUSUARIO ="id_usuario";

    final String CREAR_TABLA_USUARIOS="CREATE TABLE "+TABLA_USUARIOS+
            '('+KEY_ID_USUARIO+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            +KEY_CORREO+" TEXT,"
            +KEY_NOMBRE+" TEXT,"
            +KEY_APELLIDO+" TEXT,"
            +KEY_FECHA_NACIMIENTO+" TEXT,"
            +KEY_ESTATURA+" REAL,"
            +KEY_CLAVE+" TEXT"+')';

    final String CREAR_TABLA_EVALUACIONES="CREATE TABLE "+TABLA_EVALUACIONES+
            '('+KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT ,"
            +KEY_PESO+ " INTEGER,"
            +KEY_IMC+" REAL,"
            +KEY_FECHA+" TEXT,"
            +KEY_IDUSUARIO+" INTEGER"+')';




    public ConexionSqLiteHelper(@Nullable Context context) {
        super(context, NOMBRE_BD, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREAR_TABLA_USUARIOS);
        db.execSQL(CREAR_TABLA_EVALUACIONES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
