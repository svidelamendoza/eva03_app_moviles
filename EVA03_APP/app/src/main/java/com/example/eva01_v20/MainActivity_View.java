package com.example.eva01_v20;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva01_v20.adapter.EvaluacionesAdapter;
import com.example.eva01_v20.data.ConexionSqLiteHelper;
import com.example.eva01_v20.data.dao.EvaluacionDao;

import java.util.Calendar;

public class MainActivity_View extends AppCompatActivity implements View.OnClickListener {

    Cursor evaluaciones;
    RecyclerView mRecyclerView;
    EvaluacionesAdapter evaluacionesAdapter;
    RecyclerView.LayoutManager manager;
    EvaluacionDao evaluacionDao;
    TextView tvFechaDesde;
    TextView tvFechaHasta;
    DatePickerDialog.OnDateSetListener fechaDesdeListener;
    DatePickerDialog.OnDateSetListener fechaHastaListener;
    String fechaDesde;
    String fechaHasta;
    Button btnBuscar;
    Button btnTodos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__view);

        cargaEva();
        tvFechaDesde = findViewById(R.id.tvFechaDesde);
        tvFechaHasta = findViewById(R.id.tvFechaHasta);

        tvFechaDesde.setOnClickListener(this);
        tvFechaHasta.setOnClickListener(this);

        btnBuscar = findViewById(R.id.btnBuscar);
        btnTodos = findViewById(R.id.btnTodos);

        btnBuscar.setOnClickListener(this);
        btnTodos.setOnClickListener(this);

        mRecyclerView = findViewById(R.id.recyclerView2);
        mRecyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(this);
        evaluacionesAdapter = new EvaluacionesAdapter(this, evaluaciones);


        mRecyclerView.setLayoutManager(manager);
        mRecyclerView.setAdapter(evaluacionesAdapter);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                eliminaEvaluacion((int) viewHolder.itemView.getTag());
            }
        }).attachToRecyclerView(mRecyclerView);

        fechaDesdeListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month+=1;
                fechaDesde = String.valueOf(year).concat("/").concat(String.valueOf(month)).concat("/").concat(String.valueOf(day));
                tvFechaDesde.setText("Fecha Desde: "+fechaDesde);
            }
        };

        fechaHastaListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month+=1;
                fechaHasta = String.valueOf(year).concat("/").concat(String.valueOf(month)).concat("/").concat(String.valueOf(day));
                tvFechaHasta.setText("Fecha Hasta: "+fechaHasta);
            }
        };
    }

    public void eliminaEvaluacion(int id) {
        evaluacionDao = new EvaluacionDao(getApplicationContext());
        if (evaluacionDao.deleteEvaluacion(id)) {
            cargaEva();
            evaluacionesAdapter.swapCursor(evaluaciones);
            Toast.makeText(this, "Registro eliminado correctamente", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Problema al eliminar el registro", Toast.LENGTH_SHORT).show();
        }

    }

    public void cargaEvaluaciones() {
        evaluacionDao = new EvaluacionDao(getApplicationContext());
        //evaluaciones = evaluacionDao.getEvaluaciones();
    }

    private void cargaRango(String fechaDesde, String fechaHasta) {

        Toast.makeText(this, "fecha desde: " + fechaDesde + "- fecha hasta: " + fechaHasta, Toast.LENGTH_SHORT).show();

        ConexionSqLiteHelper conn = new ConexionSqLiteHelper(getApplicationContext());
        SQLiteDatabase db = conn.getReadableDatabase();

        String[] parametros = {fechaDesde,fechaHasta};
        String[] campos = {"id","peso","imc","fecha","id_usuario"};
        Cursor cursor = db.query("evaluaciones",campos,"fecha BETWEEN ? AND ? ",parametros,null,null,"fecha");

        evaluaciones = cursor;
        evaluacionesAdapter.swapCursor(evaluaciones);
    }

    public void cargaEva() {
        Toast.makeText(this, "Carga Todas", Toast.LENGTH_SHORT).show();
        ConexionSqLiteHelper conn = new ConexionSqLiteHelper(getApplicationContext());
        SQLiteDatabase db = conn.getReadableDatabase();

        Cursor cursor = db.query("evaluaciones", null, null, null, null, null, null);


        evaluaciones = cursor;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnTodos:
                cargaEva();
                break;
            case R.id.btnBuscar:
                cargaRango(fechaDesde,fechaHasta);
                break;
            case R.id.tvFechaDesde:
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog  datePickerDialog = new DatePickerDialog(this,
                        fechaDesdeListener,
                        year,month,dayOfMonth);
                datePickerDialog.show();
                break;
            case R.id.tvFechaHasta:
                Calendar calendar = Calendar.getInstance();
                int yearHasta = calendar.get(Calendar.YEAR);
                int monthHasta = calendar.get(Calendar.MONTH);
                int dayOfMonthHasta = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog  datePickerDialogHasta = new DatePickerDialog(this,
                        fechaHastaListener,
                        yearHasta,monthHasta,dayOfMonthHasta);
                datePickerDialogHasta.show();
                break;
        }
    }
}