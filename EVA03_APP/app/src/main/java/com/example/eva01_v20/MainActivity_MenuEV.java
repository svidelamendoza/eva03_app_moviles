package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.UsuarioDao;
import com.example.eva01_v20.data.entidades.Usuario;

public class MainActivity_MenuEV extends AppCompatActivity {

    TextView lastimc;
    TextView lastdate;
    TextView lastweight;
    String correo;
    Usuario usuario;
    double estatura;

    //Ver último registro ingresado
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__menu_e_v);

        /*lastimc = findViewById(R.id.txtlastIMC);
        lastdate = findViewById(R.id.txtlastdate);
        lastweight = findViewById(R.id.txtlastweight);
*/
        correo = getIntent().getExtras().getString("correo");

        obtenerUsuario(correo);

    }

    private void obtenerUsuario(String correo) {
        UsuarioDao usuarioDao = new UsuarioDao(getApplicationContext());
        usuario = usuarioDao.getUsuarioPorEmail(getApplicationContext(),correo);

        if(!(usuario == null)){
            estatura = usuario.getEstatura();
        }else{
            Toast.makeText(this, "Instancia de usuario no existe", Toast.LENGTH_SHORT).show();
        }
    }

    //Método para ir a Crear Registro
    public void Crear (View view){
        Intent crear = new Intent(this, MainActivity_RegisterEV.class);
        crear.putExtra("correo",correo);
        startActivity(crear);
    }

    //Método para ir a Buscar Registro
    public void Buscar (View view){
        Intent buscar = new Intent(this, MainActivity_View.class);
        startActivity(buscar);
    }

    //Método para Ver todas
    public void Ver (View view){
    Intent ver = new Intent(this, MainActivity_All.class);
    startActivity(ver);
    }

    // Método para Eliminar
    public void Eliminar (View view){
        Intent eliminar = new Intent(this, MainActivity_DeleteEV.class);
        startActivity(eliminar);
    }
}

