package com.example.eva01_v20.data.entidades;

public class Evaluacion {
    private int id;
    private int peso;
    private double imc;
    private String fecha;
    private int id_usuario;

    public Evaluacion() {
    }

    public Evaluacion(int id, int peso, double imc, String fecha, int id_usuario) {
        this.id = id;
        this.peso = peso;
        this.imc = imc;
        this.fecha = fecha;
        this.id_usuario = id_usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public double getImc() {
        return imc;
    }

    public void setImc(double imc) {
        this.imc = imc;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }
}
