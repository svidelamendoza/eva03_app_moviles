package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.example.eva01_v20.data.dao.EvaluacionDao;
import com.example.eva01_v20.data.entidades.Evaluacion;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity_All extends AppCompatActivity {

    String id_usuario;
    ArrayList<Evaluacion> evaluaciones;
    ArrayList<String> listaInformacion;
    ListView registros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__all);

        // id_usuario = getIntent().getExtras().getString("id");
        consultarEvaluaciones();

        registros = findViewById(R.id.txtviews);

        ArrayAdapter adapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_expandable_list_item_1,listaInformacion);
        registros.setAdapter(adapter);

    }

    private void consultarEvaluaciones() {
        EvaluacionDao evaluacionDao = new EvaluacionDao(getApplicationContext());
        evaluaciones = evaluacionDao.getEvaluaciones();

        obtenerLista();
    }

    private void obtenerLista() {
        listaInformacion = new ArrayList<String>();

        for(int i = 0;i<evaluaciones.size();i++){
            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            String imc = decimalFormat.format(evaluaciones.get(i).getImc());
            listaInformacion.add("Peso: "+evaluaciones.get(i).getPeso()+
                    " - IMC: "+imc+
                    "- Fecha:"+evaluaciones.get(i).getFecha());
        }

    }


}